# Brief description
Flow conditions refers to an experience similar to the 'Choose Your Own Adventure' books from the early 1980s implying diversion, too.

## Please visit
You can see [the project](https://artavia.gitlab.io/flow_con "the project at github") for yourself and decide whether it is really for you or not. 

### What is new inside?
I was juggling as you can see 30 distinct pages and wanted to experiment with something for publication with a template other than narrow&#45;jumbotron, hence, the two other templates that I settled on. I chose __*cover*__ for its simplistic tones and __*justified-nav*__ so that I could have some JQuery tooltipping goodness baked in. You will notice I opted not to copy over these resources with npm, rather, I created a __*SASS*__ file for each different template. The customized static variables were only applied to new code and not to the sassified originals. I also did not want to deal with a multitude at once which would have equated to a CSS cascade nightmare. Thus, I decided to make exceptions to a single design and I believe it turned out pretty nice, too.

### What is needed to run this?
We are going to need a few items to make this work and to address any gaping dependencies. Here are those items:
- JQuery ~ you can pull Twitter Bootstrap by typing __*bower init bootstrap --save-dev*__ in normal cases, then, fish for your copy of JQuery from the same download;
- Tether ~ this is necessary in order to make the collapsible div work in the __*justified-nav*__ template;
- Bootstrap 4 Alpha ~ this is becoming quite the handy library;
- There are two custom directives in the __*package.json*__ that you should be concerned about:
	- &laquo; npm run bower_dev &raquo; This is for essential template functionality and will bring everything I had just mentioned above;
	- &laquo; npm run twbs4 &raquo; This brings in the necessary template assets, creates a *bower_components* directory if one does not exist, then, unload the assets required by __gulpfile.js__.
- Then, you can type __*gulp*__ in your terminal to churn away everything you just imported. This will compile the project to view in your favorite browser.

#### Future considerations
Initially, this work consisted of mechanics and functioned in B\W. Thus, after cookie functionality came what passed as styling with Bootstrap v3.3.7. Presently, I am trying to achieve glissando. A technique musicians may know achieved by releasing pressure on the strings while hastily changing tones in range of a &half; to whole tone at a time. To a boxer such as Sugar Ray Leonard (Sugar Ray Robinson was eons before my time), it could mean something extra special with his dancing feet right before some sort of climatic moment achieved through some form of unassuming grace. 

I can still think of several considerations from the get&#45;go possibly including any of the following in no particular order: 
- In a real life situation, the data would be handled by a server before it is __*86&rsquo;d*__ into the future;
- Web storage&#45;&#45; bar&#45;none &#45;&#45;in my experience will cull all instances of FOUC (acronym for flash of unformatted content);
- Perhaps choose a theme other than fictional pirates;
- Page transitions not as a result of flash of unformatted content and introduce multilingual operability&#45;&#45; the latter which is a subject for another time.
- RequireJS is the big one. Obviously, RequireJS will address the code brittleness you will find buried in gulpfile.js as it corresponds to the javascript sources. 

##### The MARINEs are coming for you
Who&rsquo;d ever thought that the MARINEs can be capable of such trolling?! 

###### A dedication
This is dedicated to those of you that want to be a fictional pirate for a day ;) !!!