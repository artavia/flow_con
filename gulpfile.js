var gulp = require('gulp');
var del = require( 'del' );
var sass = require('gulp-sass'); 
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var connect = require('gulp-connect');
var autoprefixer = require('gulp-autoprefixer');
var notify = require( 'gulp-notify' );
var sourcemaps = require( 'gulp-sourcemaps' );
var newer = require( 'gulp-newer' );
var ghPages = require('gulp-gh-pages');

var paths = {

  cleanItems: [ 
    'js/',
    'css/',
    'styles/', 
    // 'scripts/ie10-viewport-bug-workaround.js', 
    'scripts/jquery.min.js' ,
    'scripts/tether.min.js' , 
    'scripts/bootstrap.min.js' 
  ] ,

  indexSrc: 'index.html', 
  sassSrc : [ 'sass/**/*.scss' ] , 
  stylesSrc: 'styles/**/*.css' , 
  stylesFldr: 'styles', 
  cssDstFldr: 'css', 
  cssprelimNm: 'all.css', 
  cssDstFile: 'all.min.css', 

  // scriptsSrc: [ 'scripts/**/*.js' ] , 
  scriptsSrc: [ 
    'scripts/complete.js', 
    // 'scripts/ie10-viewport-bug-workaround.js', 
    'scripts/jquery.min.js', 
    'scripts/tether.min.js' ,
    'scripts/bootstrap.min.js' 
  ] , 

  jssrcFold: 'scripts', 
  jsdestFold: 'js', 
  jsprelimNm: 'all.js', 
  jsdestName: 'all.min.js' , 

  /* // BOOTSTRAP version 4
  stylesBowerSrc: [ 
    'bower_components/bootstrap-4-dev/docs/dist/css/bootstrap.min.css'
  ] , 

  // BOOTSTRAP version 4 
  scriptsBowerSrc: [ 
    'bower_components/bootstrap-4-dev/docs/assets/js/ie10-viewport-bug-workaround.js' ,
    'bower_components/jquery/dist/jquery.min.js', 
    'bower_components/tether/dist/js/tether.min.js' ,
    'bower_components/bootstrap-4-dev/dist/js/bootstrap.min.js' 
  ]  */

  // BOOTSTRAP version 4 ~ new version as of 06/08/2018
  stylesBowerSrc: [ 
    'bower_components/bootstrap-4-dev/dist/css/bootstrap.min.css'
  ] , 

  // BOOTSTRAP version 4 ~ new version as of 06/08/2018
  scriptsBowerSrc: [ 
    // 'bower_components/bootstrap-4-dev/docs/assets/js/ie10-viewport-bug-workaround.js' ,
    'bower_components/jquery/dist/jquery.min.js', 
    'bower_components/tether/dist/js/tether.min.js' ,
    'bower_components/bootstrap-4-dev/dist/js/bootstrap.min.js' 
  ] 
}; 

gulp.task( 'watch', function() {
  gulp.watch( paths.indexSrc , ['html']);
  gulp.watch( paths.scriptsSrc , ['scripts']);
  gulp.watch( paths.sassSrc , ['styles']);
});

gulp.task('connect', function() {
  connect.server( { livereload: true } );
} );

gulp.task('clean', function() {
  return del( paths.cleanItems );
} );

gulp.task( 'importCssBwr', function() {
  gulp.src( paths.stylesBowerSrc ).pipe( gulp.dest( paths.stylesFldr ) );
});

gulp.task( 'importJsBwr', function() {
  gulp.src( paths.scriptsBowerSrc ).pipe( gulp.dest( paths.jssrcFold ) );
});

gulp.task('html', function() {
  gulp.src( paths.indexSrc )
    .pipe( connect.reload() )
    .pipe( notify( { message: 'html task complete and reloaded' } ) );
});

gulp.task('scripts', function() {
  return gulp.src( paths.scriptsSrc )
    .pipe( sourcemaps.init() )
    .pipe( concat( paths.jsprelimNm ) )
    .pipe( rename( paths.jsdestName ) )
    .pipe( uglify() )
    .pipe( sourcemaps.write() )
    .pipe( gulp.dest( paths.jsdestFold ) )
    .pipe( connect.reload() )
    .pipe( notify( { message: 'Scripts task complete and reloaded' } ) );
});

gulp.task('sass', function() {
  return gulp.src( paths.sassSrc )
    .pipe( sourcemaps.init() )
    .pipe( sass().on( 'error', sass.logError ) )
    .pipe( autoprefixer( { browsers: ['last 2 versions'], cascade: false } ) )
    .pipe( sourcemaps.write() )
    .pipe( gulp.dest( paths.stylesFldr ) )
    .pipe( notify( { message: 'Sass subtask complete' } ) );
});

gulp.task('styles', ['sass'], function() { 
  return gulp.src( paths.stylesSrc )
    .pipe( concat( paths.cssprelimNm ) )
    .pipe( rename( paths.cssDstFile ) )
    .pipe( cssnano() )
    .pipe( gulp.dest( paths.cssDstFldr ) )
    .pipe( connect.reload() )
    .pipe( notify( { message: 'Styles task complete and reloaded' } ) );
});

gulp.task( 'deploy', [ 'importJsBwr', 'scripts', 'importCssBwr', 'styles' ], function() { 
  gulp.src( [ 'js/all.min.js', 'css/all.min.css', 'index.html', 'favicon.png' ], { base: './' } )
  .pipe( ghPages() );
});

gulp.task( 'default' , [ 'clean' ], function() {
  // gulp.start( 'connect', 'watch', 'importJsBwr', 'scripts', 'importCssBwr', 'styles' ); 
  gulp.start( 'connect', 'importJsBwr', 'scripts', 'importCssBwr', 'styles' ); 
});