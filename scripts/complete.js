var DRYOBJ = ( function ( ) {
	
	'use strict';	

	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== 'DOMContentLoaded') { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				nodeFlanders.attachEvent( 'on' + type, callback );
			} 		
			else { 
				nodeFlanders['on' + type] = callback; 
			}
		}
		else 
		if( type === 'DOMContentLoaded' ) { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === 'loading' ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				nodeFlanders['on' + type] = callback; 
			}
		}
	}

	function _RetEVTsrcEL_evtTarget( leEvt ) { 
		if( typeof leEvt !== 'undefined') { 
			var _EREF = leEvt; 
		}
		else {
			var _EREF = window.event; 
		}
		if( typeof _EREF.target !== 'undefined') {
			var evtTrgt = _EREF.target;	
		}
		else {
			var evtTrgt = _EREF.srcElement; 
		}
		return evtTrgt;
	}

	function _RetValFalprevDef( leEvt ) { 
		if( typeof leEvt !== 'undefined') {
			var _EREF = leEvt; 
		} 
		else {
			var _EREF = window.event; 
		}		
		if( _EREF.preventDefault) {
			_EREF.preventDefault(); 
		}
		else {
			_EREF.returnValue = false; 
		}
	}

	// END of _private properties 
	return { 

		Utils : { 
			evt_u : {
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} , 
				RetEVTsrcEL_evtTarget : function( leEvt ) {
					return _RetEVTsrcEL_evtTarget( leEvt );
				}
				, 
				RetValFalprevDef : function( leEvt ) { 
					return _RetValFalprevDef( leEvt );
				} 
			} 
		} 

	}; // END public properties
	
}( ) ); 



var Gh_pages = ( function() {
	'use strict';

	var _docObj = window.document; // like Downy... ancient Chinese secret!

	var _str_prtcl = window.location.protocol;
	var _str_host = window.location.host;	// localhost:8080 ~ artavia.gitlab.io
	var _str_href = window.location.href;	
	var _str_url = window.location.pathname;
	var _actv_Page = _str_url.substring( _str_url.lastIndexOf('/') + 1 ); 
	var _str_Urlstring = '';
	var CNFG_SVR_IDX = _str_Urlstring;
	var _dtObj_fullYear = new Date().getFullYear();

	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  GENERIC  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	
	var Marine_confObj = {
		alerts: {
			majorTroll: 'I lied BUG-BRAIN-- fill it out!',
			cookiesOn: 'Go into your device settings, \nand, turn on cookies during this experience. \n\n Thank you!'
		}
	};

	// ----------  ConfirmField_VALUE   ----------  
	var ar_formOBJ, formMax;
	var COOKIE_LIFE = 1;

	function _DOMCONLO() {
		if( ( _actv_Page === CNFG_SVR_IDX ) || ( _actv_Page === 'index.html' ) || ( _actv_Page === '2_0_fleetService_secured.html' ) || ( _actv_Page === '2_2_in_fifthBranch.html' ) || ( _actv_Page === '3_0_bounty_only_secured.html' ) || ( _actv_Page === '4_0_createBounty.html' ) || ( _actv_Page === '4_1_yes_createBounty.html' ) || ( _actv_Page === '4_2_yes_fleetService_createBounty.html' ) || ( _actv_Page === '4_2_1_in_EniesLobby.html' ) || ( _actv_Page === '4_4_no_createBounty.html' ) || ( _actv_Page === '4_5_1_1_yes_i_am_enlisted.html' ) || ( _actv_Page === '4_5_1_2_no_i_am_not_enlisted.html' ) ) {

			if( _docObj.classList === undefined ) {
				_docObj.querySelector( 'body' ).className = ( 'cover' );
			}
			else {
				_docObj.querySelector( 'body' ).classList.add( 'cover' );
			}
		}
		else 
		if( ( _actv_Page === 'index_0_0.html' ) || ( _actv_Page === '1_0_secured.html' ) || ( _actv_Page === '2_1_in_EniesLobby.html' ) || ( _actv_Page === '2_2_1_EniesLobby.html' ) || ( _actv_Page === '2_2_2_fifthBranch.html' ) || ( _actv_Page === '3_1_in_EniesLobby.html' ) || ( _actv_Page === '3_2_in_fifthBranch.html' ) || ( _actv_Page === '4_1_1_yes_i_am_enlisted.html' ) || ( _actv_Page === '4_1_2_no_i_am_not_enlisted.html' ) || ( _actv_Page === '4_2_1_1_yes_i_am_enlisted.html' ) || ( _actv_Page === '4_2_1_2_no_I_am_not_enlisted.html' ) || ( _actv_Page === '4_2_2_in_fifthBranch.html' ) || ( _actv_Page === '4_3_yes_bounty_only_createBounty.html' ) || ( _actv_Page === '4_4_1_yes_i_am_enlisted.html' ) || ( _actv_Page === '4_4_2_i_m_not_enlisted.html' ) || ( _actv_Page === '4_5_no_fleetService_createBounty.html' )  || ( _actv_Page === '4_5_1_in_EniesLobby.html' ) || ( _actv_Page === '4_5_2_in_fifthBranch.html' ) || ( _actv_Page === '4_6_no_stockRefer_only_createBounty.html' ) ) {

			if( _docObj.classList === undefined ) {
				_docObj.querySelector( 'body' ).className = ( 'justified' );
			}
			else {
				_docObj.querySelector( 'body' ).classList.add( 'justified' );
			}
		}
		_SecureTheArea();
	} // END _DOMCONLO

	function _SecureTheArea() {	
		var yourOverlords = 'Marine Registration Processing' ;
		var ourTurf = 'Grand Line' ; 
		var copysymbol = '\u00A9'; 
		var liteStr = ' ' + _dtObj_fullYear + ' ';
		var fullStr = ' ' + ourTurf + liteStr + yourOverlords;
		var xtraStr = copysymbol + fullStr; 
		
		if( ( _docObj.querySelector( 'body' ).className === ( 'justified' ) ) ) {

			var _footerishELs = _docObj.getElementsByTagName('footer');
			var _ftrMax = _footerishELs.length;
			var xxA = _ftrMax - 1; 
			for (xxA = 0; xxA < _ftrMax; xxA = xxA + 1) {
				if (_footerishELs[xxA].className === 'footer') {
					if( _footerishELs[xxA].children[0].tagName.toLowerCase() === 'p' ) {
						_footerishELs[xxA].children[0].innerHTML = xtraStr;
					}
				}
			}
			var _navishELs = _docObj.getElementsByTagName('div');
			var _navMax = _navishELs.length;
			var xxB = _navMax - 1;
			for (xxB = 0; xxB < _navMax; xxB = xxB + 1) {
				if (_navishELs[xxB].className === 'collapse navbar-collapse') {
					if( _navishELs[xxB].children[0].children[0].children[0].tagName.toLowerCase() === 'a' ) {						
						DRYOBJ.Utils.evt_u.AddEventoHandler( _navishELs[xxB].children[0].children[0].children[0], 'click', DRYOBJ.Utils.evt_u.RetValFalprevDef );
					}
				}
			}
		} 
		else
		if( ( _docObj.querySelector( 'body' ).className === ( 'cover' ) ) ) {
			
			var _footerishELs = _docObj.getElementsByTagName('div');			
			var _ftrMax = _footerishELs.length;
			var xxA = _ftrMax - 1;
			for (xxA = 0; xxA < _ftrMax; xxA = xxA + 1) {
				if (_footerishELs[xxA].className === 'mastfoot') {
					_footerishELs[xxA].children[0].children[0].innerHTML = xtraStr;
				}
			}
			var _navishELs = _docObj.getElementsByTagName('nav'); 
			var _navMax = _navishELs.length;
			var xxB = _navMax - 1;
			for (xxB = 0; xxB < _navMax; xxB = xxB + 1) {
				if (_navishELs[xxB].className === 'nav nav-masthead') {
					DRYOBJ.Utils.evt_u.AddEventoHandler( _navishELs[xxB].children[0], 'click', DRYOBJ.Utils.evt_u.RetValFalprevDef );
				}
			}
		} 
	} // END _SecureTheArea()
	
	function _LOAD() {
		if( ( _actv_Page === CNFG_SVR_IDX ) || ( _actv_Page === 'index.html' ) ) {
			// MAIN PAGE
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_main ); 
		}
		else 
		if( _actv_Page === 'index_0_0.html' ) {
			// index_0_0
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_zeroZero );
		}
		else 
		if( _actv_Page === '1_0_secured.html' ) {
			// index_1_0
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_oneZero );
		}
		else 
		if( _actv_Page === '2_0_fleetService_secured.html' ) {
			// index_2_0
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_twoZero );
		}
		else 
		if( _actv_Page === '2_1_in_EniesLobby.html' ) {
			// index_2_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_twoOne );
		}
		else 
		if( _actv_Page === '2_2_in_fifthBranch.html' ) {
			// index_2_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_twoTwo );
		}
		else 
		if( _actv_Page === '2_2_1_EniesLobby.html' ) {
			// index_2_2_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_twoTwoOne ); 
		}
		else 
		if( _actv_Page === '2_2_2_fifthBranch.html' ) {
			// index_2_2_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_twoTwoTwo ); 
		}
		else 
		if( _actv_Page === '3_0_bounty_only_secured.html' ) {
			// index_3_0
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_threeZero );
		}
		else 
		if( _actv_Page === '3_1_in_EniesLobby.html' ) {
			// index_3_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_threeOne ); 
		}
		else 
		if( _actv_Page === '3_2_in_fifthBranch.html' ) {
			// index_3_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_threeTwo ); 
		}
		else 
		if( _actv_Page === '4_0_createBounty.html' ) {
			// index_4_0
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourZero );
		}
		else 
		if( _actv_Page === '4_1_yes_createBounty.html' ) {
			// index_4_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourOne );
		}
		else 
		if( _actv_Page === '4_1_1_yes_i_am_enlisted.html' ) {
			// index_4_1_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourOneOne ); 
		}
		else 
		if( _actv_Page === '4_1_2_no_i_am_not_enlisted.html' ) {
			// index_4_1_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourOneTwo ); 
		}
		else 
		if( _actv_Page === '4_2_yes_fleetService_createBounty.html' ) {
			// index_4_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourTwo );
		}
		else 
		if( _actv_Page === '4_2_1_in_EniesLobby.html' ) {
			// index_4_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourTwoOne );
		}
		else 
		if( _actv_Page === '4_2_1_1_yes_i_am_enlisted.html' ) {
			// index_4_2_1_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourTwoOneOne ); 
		}
		else 
		if( _actv_Page === '4_2_1_2_no_I_am_not_enlisted.html' ) {
			// index_4_2_1_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourTwoOneTwo ); 
		}
		else 
		if( _actv_Page === '4_2_2_in_fifthBranch.html' ) {
			// index_4_2_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourTwoTwo );
		}
		else 
		if( _actv_Page === '4_3_yes_bounty_only_createBounty.html' ) {
			// index_4_3
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourThree );
		}
		else 
		if( _actv_Page === '4_4_no_createBounty.html' ) {
			// index_4_4
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFour );
		}
		else 
		if( _actv_Page === '4_4_1_yes_i_am_enlisted.html' ) {
			// index_4_4_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFourOne ); 
		}
		else 
		if( _actv_Page === '4_4_2_i_m_not_enlisted.html' ) {
			// index_4_4_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFourTwo ); 
		}
		else 
		if( _actv_Page === '4_5_no_fleetService_createBounty.html' ) {
			// index_4_5
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFive );
		}
		else 
		if( _actv_Page === '4_5_1_in_EniesLobby.html' ) {
			// index_4_5_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFiveOne );
		}
		else 
		if( _actv_Page === '4_5_1_1_yes_i_am_enlisted.html' ) {
			// index_4_5_1_1
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFiveOneOne ); 
		}
		else 
		if( _actv_Page === '4_5_1_2_no_i_am_not_enlisted.html' ) {
			// index_4_5_1_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFiveOneTwo ); 
		}
		else 
		if( _actv_Page === '4_5_2_in_fifthBranch.html' ) {
			// index_4_5_2
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourFiveTwo );
		}
		else 
		if( _actv_Page === '4_6_no_stockRefer_only_createBounty.html' ) {
			// index_4_6
			DRYOBJ.Utils.evt_u.AddEventoHandler( self, "load", Init_fourSix ); 
		}
	} // END _LOAD
	
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  ConfirmField_VALUE  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	function ConfirmField_VALUE( pm1 ) {
		if ( pm1.checked ) {
			return true;
		}
		return false;
	}
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Go_Home  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	function Go_Home(leEvt) { 

		DRYOBJ.Utils.evt_u.RetValFalprevDef(leEvt); 	
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE );
		window.alert( "Thank you, " + jollyrzerovalue + "! \nYou will now be re-directed \nto the next page. ");

		if (JOLLYRZEROVALUE ) { ToastCookie( JOLLYRZEROVALUE ); }
		if ( PAGE_0_0_VALUE_1 ) { ToastCookie( PAGE_0_0_VALUE_1 ); }
		if ( PAGE_0_0_VALUE_2 ) { ToastCookie( PAGE_0_0_VALUE_2 ); }
		if ( PAGE_2_0_VALUE_3 ) { ToastCookie( PAGE_2_0_VALUE_3 ); }
		if ( PAGE_2_2_VALUE_4 ) { ToastCookie( PAGE_2_2_VALUE_4 ); }
		if ( PAGE_3_0_VALUE_5 ) { ToastCookie( PAGE_3_0_VALUE_5 ); }
		if ( PAGE_4_0_VALUE_6 ) { ToastCookie( PAGE_4_0_VALUE_6 ); }
		if ( PAGE_4_1_VALUE_7 ) { ToastCookie( PAGE_4_1_VALUE_7 ); }
		if ( PAGE_4_2_VALUE_8 ) { ToastCookie( PAGE_4_2_VALUE_8 ); }
		if ( PAGE_4_2_1_VALUE_9 ) { ToastCookie( PAGE_4_2_1_VALUE_9 ); }
		if ( PAGE_4_4_VALUE_10 ) { ToastCookie( PAGE_4_4_VALUE_10 ); }
		if ( PAGE_4_5_VALUE_11 ) { ToastCookie( PAGE_4_5_VALUE_11 ); }
		if ( PAGE_4_5_1_VALUE_12 ) { ToastCookie( PAGE_4_5_1_VALUE_12 ); }

		if( (_str_host === 'localhost') || (_str_host === 'localhost:8080') ) {
			var Steve_Austin_Bionic_URL = _str_prtcl + '//' + _str_host + '/';
			window.location.href = Steve_Austin_Bionic_URL;
		}
		else 
		if( _str_host === 'artavia.gitlab.io') {
			var subst_pathname = 'flow_con/';
			var Steve_Austin_Bionic_URL = _str_prtcl + '//' + _str_host + '/' + subst_pathname;	
			window.location.href = Steve_Austin_Bionic_URL;
		}
	}

	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ goBack  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	function goBack() {	
		window.history.go( -1 );
	} 
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ ToastCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	function ToastCookie( pmNamecookie ) { 
		SetCookie( pmNamecookie , "", -1 ); 
	} 
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ SetCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	function SetCookie( pmNamecookie, pmValcookie, pmDayscookie ) {
		if ( pmDayscookie ) {
			var expDate = new Date();
			expDate.setTime( expDate.getTime() + ( pmDayscookie * 24*60*60*1000 ) );
			var str_Expires = "; expires=" + expDate.toGMTString();
			_docObj.cookie = pmNamecookie + "=" + pmValcookie + str_Expires + "; path=/";
		}
		else {
			var str_Expires = "";
			_docObj.cookie = pmNamecookie + "=" + pmValcookie + str_Expires +"; path=/";
		}
	}
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ GetCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	function GetCookie( pmNamecookie ) {
		var getCookieString = pmNamecookie + "=";
		var ar_Cookies = _docObj.cookie.split(';');
		var arMax = ar_Cookies.length;
		var dL; 
		for( dL = 0; dL < arMax; dL = dL + 1 ) {
			var cookieName = ar_Cookies[ dL ];
			while (cookieName.charAt( 0 ) === ' ' ) cookieName = cookieName.substring( 1, cookieName.length );
			if ( cookieName.indexOf( getCookieString ) === 0 ) return cookieName.substring( getCookieString.length, cookieName.length );
		}
		return null;
	}
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  MAIN PAGE  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	// ----------  MAINPAGE   ----------  
	var ar_introForm, introMax, nmfldOBJ = _docObj.getElementById( "fullName" );
	var JOLLYRZEROVALUE = "jollyRZero";
	var jollyRZero = "";
	var participant, a = null;

	function Init_main() { 
		if( navigator.cookieEnabled ) {
			ar_introForm = _docObj.getElementsByTagName( "form" ), introMax = ar_introForm.length; 
			for ( a = 0; a < introMax; a = a + 1 ) { 
				if( ar_introForm[a].id === "formalInvitation" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_introForm[a], "submit", Gliss_main );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn );
		}
	}
	function Gliss_main( leEvt ) { 
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt ); 
		participant = ConfirmMain( nmfldOBJ ); 
		if( participant ) { 
			jollyRZero = nmfldOBJ.value; // ( AKA jollyRZero )
			// console.log( "PIRATE or SAILOR?: " + jollyRZero + "; " ); 
			window.alert( "Thank you, " + jollyRZero + "! \nYou will now be re-directed \nto the next page. "); 
			SetCookie( JOLLYRZEROVALUE, jollyRZero, COOKIE_LIFE );
			window.location.href = "warship/index_0_0.html";
		}
		else 
		if( !participant ) {
			window.alert(Marine_confObj.alerts.majorTroll); 
		}
	} 
	function ConfirmMain( pm1 ) {
		if ( pm1.value !== "" ) {
			return true;
		}
		return false;
	} 
	//   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_0_0  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
	// ----------  0_0   ----------  
	var PAGE_0_0_VALUE_1 = "value1", PAGE_0_0_VALUE_2 = "value2", jollyrzerovalue;
	var value1 = "", value2 = ""; 
	var butThereIsAnIinWin1, butThereIsAnIinWin2, b = null;

	function Init_zeroZero( ) { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE );
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; " ); 		
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( b = 0; b < formMax; b = b + 1 ) { 
				if( ar_formOBJ[b].id === "firstSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[b], "submit", Gliss_zeroZero );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_zeroZero( leEvt ) { 
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt ); 
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE );	
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 	
		if( evtTrgt.id === "firstSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, c = null;
			for( c = 0; c < inputMax; c = c + 1) {
				butThereIsAnIinWin1 = ConfirmField_VALUE( ar_InputRadio[ c ] ); 
				butThereIsAnIinWin2 = ConfirmField_VALUE( ar_InputRadio[ c ] ); 
				if( butThereIsAnIinWin1 && ar_InputRadio[ c ].className === "radioOne" ) { 
					value1 = ar_InputRadio[ c ].value; // ( AKA processingStatus ) 
					SetCookie( PAGE_0_0_VALUE_1, value1, COOKIE_LIFE );
				}
				if( butThereIsAnIinWin2 && ar_InputRadio[ c ].className === "radioTwo" ) { 
					value2 = ar_InputRadio[ c ].value; // ( AKA maritimeHabits )
					SetCookie( PAGE_0_0_VALUE_2, value2, COOKIE_LIFE ); 
				}
			} // END for LOOP
			if( value1.length > 0 === true && value2.length > 0 === true ) {
				if( value1 === "registered" && value2 === "locatedAtHome" ) { 
					window.location.href = "quarterDeck_1/1_0_secured.html";
				} 
				else 
				if( value1 === "registered" && value2 === "outAtSea" ) { 
					window.location.href = "quarterDeck_2/2_0_fleetService_secured.html";
				} 
				else 
				if( value1 === "registered" && value2 === "inquireFurther" ) { 
					window.location.href = "quarterDeck_3/3_0_bounty_only_secured.html";
				} 
				else 
				if( value1 === "pirateWatch" && value2 === "locatedAtHome" ) { 
					window.location.href = "quarterDeck_4/4_0_createBounty.html";
				} 
				else 
				if( value1 === "pirateWatch" && value2 === "outAtSea" ) { 
					window.location.href = "quarterDeck_4/4_0_createBounty.html";
				} 
				else 
				if( value1 === "pirateWatch" && value2 === "inquireFurther" ) { 
					window.location.href = "quarterDeck_4/4_0_createBounty.html";
				} 
			}
			else
			if( ( value1.length > 0 !== true && value2.length > 0 !== true ) || ( value1.length > 0 === true && value2.length > 0 !== true ) || ( value1.length > 0 !== true && value2.length > 0 === true ) ) {
				window.alert("Fill out both values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_1_0  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  0_0   ----------  
	var d = null;

	function Init_oneZero() { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( d = 0; d < formMax; d = d + 1 ) { 
				if( ar_formOBJ[d].id === "GoHome_OneTopLEV" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[d], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_2_0  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  2_0   ----------  
	var page_0_0_value_1, page_0_0_value_2;
	var PAGE_2_0_VALUE_3 = "value3";
	var value3 = "";
	var butThereIsAnIinWin3, e = null;

	function Init_twoZero() { 
		if( navigator.cookieEnabled ) {
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( e = 0; e < formMax; e = e + 1 ) { 
				if( ar_formOBJ[e].id === "secondSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[e], "submit", Gliss_twoZero );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn );
		}
	}
	function Gliss_twoZero( leEvt ) { 

		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( evtTrgt.id === "secondSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, f = null;
			for( f = 0; f < inputMax; f = f + 1) {
				butThereIsAnIinWin3 = ConfirmField_VALUE( ar_InputRadio[ f ] ); 
				if( butThereIsAnIinWin3 && ar_InputRadio[ f ].className === "radioThree" ) {
					value3 = ar_InputRadio[ f ].value; // ( AKA sailors )
					SetCookie( PAGE_2_0_VALUE_3, value3, COOKIE_LIFE );
				} 
			} // END for LOOP
			if( value3.length > 0 === true ) {
				if( value3 === "outAtSea" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_2_1/2_1_in_EniesLobby.html";
				} 
				else 
				if( value3 === "inquireFurther" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_2_2/2_2_in_fifthBranch.html";
				} 					
			}
			else
			if( value3.length > 0 !== true  ) {
				// window.alert("Fill out one of the values now.");
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_2_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  2_2   ----------  
	var PAGE_2_2_VALUE_4 = "value4";
	var value4 = "";
	var butThereIsAnIinWin4, g = null;
	var page_2_0_value_3;

	function Init_twoTwo() { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_2_0_value_3 = GetCookie(PAGE_2_0_VALUE_3);
			
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; sailors: " + page_2_0_value_3 + "; " ); 
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( g = 0; g < formMax; g = g + 1 ) { 
				if( ar_formOBJ[g].id === "thirdSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[g], "submit", Gliss_twoTwo );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_twoTwo( leEvt ) { 

		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_2_0_value_3 = GetCookie(PAGE_2_0_VALUE_3);
		
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; sailors: " + page_2_0_value_3 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( evtTrgt.id === "thirdSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, h = null;
			for( h = 0; h < inputMax; h = h + 1) {
				butThereIsAnIinWin4 = ConfirmField_VALUE( ar_InputRadio[ h ] ); 
				if( butThereIsAnIinWin4 && ar_InputRadio[ h ].className === "radioFour" ) {
					value4 = ar_InputRadio[ h ].value; // ( AKA recruit )
					SetCookie( PAGE_2_2_VALUE_4, value4, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value4.length > 0 === true ) { 
				if( value4 === "outAtSea" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "2_2_1_EniesLobby.html";
				} 
				else 
				if( value4 === "inquireFurther" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "2_2_2_fifthBranch.html";
				} 					
			}
			else
			if( value4.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_3_0  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  3_0   ----------  
	var PAGE_3_0_VALUE_5 = "value5";
	var value5 = "";
	var butThereIsAnIinWin5, i = null;

	function Init_threeZero() { 
		if( navigator.cookieEnabled ) {
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie( PAGE_0_0_VALUE_1 );
			page_0_0_value_2 = GetCookie( PAGE_0_0_VALUE_2 );
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( i = 0; i < formMax; i = i + 1 ) { 
				if( ar_formOBJ[i].id === "fourthSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[i], "submit", Gliss_threeZero );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	function Gliss_threeZero( leEvt ) { 

		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie( PAGE_0_0_VALUE_1 );
		page_0_0_value_2 = GetCookie( PAGE_0_0_VALUE_2 );
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( evtTrgt.id === "fourthSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, j = null;
			for( j = 0; j < inputMax; j = j + 1) {
				butThereIsAnIinWin5 = ConfirmField_VALUE( ar_InputRadio[ j ] ); 
				if( butThereIsAnIinWin5 && ar_InputRadio[ j ].className === "radioFive" ) {
					value5 = ar_InputRadio[ j ].value; // ( AKA recruitment )
					SetCookie( PAGE_3_0_VALUE_5, value5, COOKIE_LIFE );
				} 
			} // END for LOOP
			if( value5.length > 0 === true ) {
				if( value5 === "outAtSea" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_3_1/3_1_in_EniesLobby.html";
				} 
				else 
				if( value5 === "inquireFurther" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_3_2/3_2_in_fifthBranch.html";
				} 					
			}
			else
			if( value5.length > 0 !== true  ) {
				// window.alert("Fill out one of the values now.");
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_0  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  4_0   ----------  
	var PAGE_4_0_VALUE_6 = "value6";
	var value6 = "";
	var butThereIsAnIinWin6, K = null;

	function Init_fourZero() { 
		if( navigator.cookieEnabled ) {
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; " ); 	
				
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( K = 0; K < formMax; K = K + 1 ) { 
				if( ar_formOBJ[K].id === "fifthSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[K], "submit", Gliss_fourZero );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourZero( leEvt ) { 
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie( PAGE_0_0_VALUE_1 );
		page_0_0_value_2 = GetCookie( PAGE_0_0_VALUE_2 );
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; " ); 	
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );	 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 	
		if( evtTrgt.id === "fifthSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, L = null;
			for( L = 0; L < inputMax; L = L + 1) {
				butThereIsAnIinWin6 = ConfirmField_VALUE( ar_InputRadio[ L ] ); 
				if( butThereIsAnIinWin6 && ar_InputRadio[ L ].className === "radioSix" ) {
					value6 = ar_InputRadio[ L ].value; // ( AKA process )
					SetCookie( PAGE_4_0_VALUE_6, value6, COOKIE_LIFE );
				} 
			} // END for LOOP		
			if( value6.length > 0 === true ) {
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "locatedAtHome" && value6 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_4_1/4_1_yes_createBounty.html";
				} 
				else
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "outAtSea" && value6 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_4_2/4_2_yes_fleetService_createBounty.html";
				} 
				else
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "inquireFurther" && value6 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_4_3/4_3_yes_bounty_only_createBounty.html";
				} 
				else 
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "locatedAtHome" && value6 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_4_4/4_4_no_createBounty.html";
				} 
				else 
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "outAtSea" && value6 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_4_5/4_5_no_fleetService_createBounty.html";
				} 
				else 
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "inquireFurther" && value6 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "subdeck_4_6/4_6_no_stockRefer_only_createBounty.html";
				} 
			}
			else
			if( value6.length > 0 !== true  ) {
				// window.alert("Fill out one of the values now.");
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  4_1   ----------  
	var page_4_0_value_6; //// ??? wtf

	var PAGE_4_1_VALUE_7 = "value7";
	var value7 = "";
	var butThereIsAnIinWin7, m = null;

	function Init_fourOne() { 
		if( navigator.cookieEnabled ) { 
		
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);	
			
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( m = 0; m < formMax; m = m + 1 ) { 
				if( ar_formOBJ[m].id === "sixthSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[m], "submit", Gliss_fourOne );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourOne( leEvt ) { 

		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
		
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		
		if( evtTrgt.id === "sixthSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, n = null;
			for( n = 0; n < inputMax; n = n + 1) {
				butThereIsAnIinWin7 = ConfirmField_VALUE( ar_InputRadio[ n ] ); 
				if( butThereIsAnIinWin7 && ar_InputRadio[ n ].className === "radioSeven" ) {
					value7 = ar_InputRadio[ n ].value; // ( AKA RecallG5enroll )
					SetCookie( PAGE_4_1_VALUE_7, value7, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value7.length > 0 === true ) { 
				if( value7 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_1_1_yes_i_am_enlisted.html";
				} 
				else 
				if( value7 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_1_2_no_i_am_not_enlisted.html";
				} 					
			}
			else
			if( value7.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// var page_4_0_value_6;
	// ----------  4_2   ----------
	var PAGE_4_2_VALUE_8 = "value8";
	var value8 = "";
	var butThereIsAnIinWin8, P = null;

	function Init_fourTwo() { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
			
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( P = 0; P < formMax; P = P + 1 ) { 
				if( ar_formOBJ[P].id === "seventhSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[P], "submit", Gliss_fourTwo );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourTwo( leEvt ) { 
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
		
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		if( evtTrgt.id === "seventhSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, Q = null;
			for( Q = 0; Q < inputMax; Q = Q + 1) {
				butThereIsAnIinWin8 = ConfirmField_VALUE( ar_InputRadio[ Q ] ); 
				if( butThereIsAnIinWin8 && ar_InputRadio[ Q ].className === "radioEight" ) {
					value8 = ar_InputRadio[ Q ].value; // ( AKA processSubject )
					SetCookie( PAGE_4_2_VALUE_8, value8, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value8.length > 0 === true ) { 
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "outAtSea" && page_4_0_value_6 === "yes" && value8 === "yes" ) { 
				// if( value8 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_2_1_in_EniesLobby.html"; 
				} 
				else 
				if( page_0_0_value_1 === "pirateWatch" && page_0_0_value_2 === "outAtSea" && page_4_0_value_6 === "yes" && value8 === "no" ) { 
				// if( value8 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_2_2_in_fifthBranch.html"; 
				} 					
			}
			else
			if( value8.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_2_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------------------------  
	var page_4_2_value_8;

	// ----------  4_2_1   ----------
	var PAGE_4_2_1_VALUE_9 = "value9";
	var value9 = "";
	var butThereIsAnIinWin9, r = null;

	function Init_fourTwoOne() { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
			page_4_2_value_8 = GetCookie(PAGE_4_2_VALUE_8);		
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; processSubject (y/n): " + page_4_2_value_8 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			
			for ( r = 0; r < formMax; r = r + 1 ) { 
				if( ar_formOBJ[r].id === "eighthSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[r], "submit", Gliss_fourTwoOne );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourTwoOne( leEvt ) { 	
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
		page_4_2_value_8 = GetCookie(PAGE_4_2_VALUE_8);		
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; processSubject (y/n): " + page_4_2_value_8 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		if( evtTrgt.id === "eighthSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, s = null;
			for( s = 0; s < inputMax; s = s + 1) {
				butThereIsAnIinWin9 = ConfirmField_VALUE( ar_InputRadio[ s ] ); 
				if( butThereIsAnIinWin9 && ar_InputRadio[ s ].className === "radioNine" ) {
					value9 = ar_InputRadio[ s ].value; // ( AKA Escort )
					SetCookie( PAGE_4_2_1_VALUE_9, value9, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value9.length > 0 === true ) { 
				if( value9 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_2_1_1_yes_i_am_enlisted.html"; 
				} 
				else 
				if( value9 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_2_1_2_no_I_am_not_enlisted.html"; 
				} 					
			}
			else
			if( value9.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_4  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  4_4   ----------  
	// var page_4_0_value_6;

	var PAGE_4_4_VALUE_10 = "value10";
	var value10 = "";
	var butThereIsAnIinWin10, t = null;

	function Init_fourFour() { 
		if( navigator.cookieEnabled ) { 	
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);		
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( t = 0; t < formMax; t = t + 1 ) { 
				if( ar_formOBJ[t].id === "ninthSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[t], "submit", Gliss_fourFour );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourFour( leEvt ) { 
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; sailors: " + page_2_0_value_3 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );	 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 	
		if( evtTrgt.id === "ninthSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, u = null;
			for( u = 0; u < inputMax; u = u + 1) {
				butThereIsAnIinWin10 = ConfirmField_VALUE( ar_InputRadio[ u ] ); 
				if( butThereIsAnIinWin10 && ar_InputRadio[ u ].className === "radioTen" ) {
					value10 = ar_InputRadio[ u ].value; // ( AKA dispatchG5 )
					SetCookie( PAGE_4_4_VALUE_10, value10, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value10.length > 0 === true ) { 
				if( value10 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_4_1_yes_i_am_enlisted.html";
				} 
				else 
				if( value10 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_4_2_i_m_not_enlisted.html";
				} 					
			}
			else
			if( value10.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_5  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  4_5   ----------  
	// var page_4_0_value_6;

	var PAGE_4_5_VALUE_11 = "value11";
	var value11 = "";
	var butThereIsAnIinWin11, v = null;

	function Init_fourFive() { 
		if( navigator.cookieEnabled ) { 	
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);		
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( v = 0; v < formMax; v = v + 1 ) { 
				if( ar_formOBJ[v].id === "tenthSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[v], "submit", Gliss_fourFive );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourFive( leEvt ) { 
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_2_0_value_3 = GetCookie(PAGE_2_0_VALUE_3);
		page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; sailors: " + page_2_0_value_3 + "; process (y/n): " + page_4_0_value_6 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );	 
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 	
		if( evtTrgt.id === "tenthSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, w = null;
			for( w = 0; w < inputMax; w = w + 1) {
				butThereIsAnIinWin11 = ConfirmField_VALUE( ar_InputRadio[ w ] ); 
				if( butThereIsAnIinWin11 && ar_InputRadio[ w ].className === "radioEleven" ) {
					value11 = ar_InputRadio[ w ].value; // ( AKA offerReward )
					SetCookie( PAGE_4_5_VALUE_11, value11, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value11.length > 0 === true ) { 
				if( value11 === "outAtSea" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_5_1_in_EniesLobby.html";
				} 
				else 
				if( value11 === "inquireFurther" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_5_2_in_fifthBranch.html";
				} 					
			}
			else
			if( value11.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_5_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// ----------  4_5_1   ----------
	var page_4_5_value_11;

	var PAGE_4_5_1_VALUE_12 = "value12";
	var value12 = "";
	var butThereIsAnIinWin12, y = null;

	function Init_fourFiveOne() { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
			page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
			page_2_0_value_3 = GetCookie(PAGE_2_0_VALUE_3);
			page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
			page_4_5_value_11 = GetCookie(PAGE_4_5_VALUE_11);		
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + "; sailors: " + page_2_0_value_3 + "; process (y/n): " + page_4_0_value_6 + "; offerReward (y/n): " + page_4_5_value_11 + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			
			for ( y = 0; y < formMax; y = y + 1 ) { 
				if( ar_formOBJ[y].id === "eleventhSet" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[y], "submit", Gliss_fourFiveOne );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	function Gliss_fourFiveOne( leEvt ) { 	
		jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
		page_0_0_value_1 = GetCookie(PAGE_0_0_VALUE_1);
		page_0_0_value_2 = GetCookie(PAGE_0_0_VALUE_2);
		page_2_0_value_3 = GetCookie(PAGE_2_0_VALUE_3);
		page_4_0_value_6 = GetCookie(PAGE_4_0_VALUE_6);
		page_4_5_value_11 = GetCookie(PAGE_4_5_VALUE_11);		
		// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; processingStatus: " + page_0_0_value_1 + "; maritimeHabits: " + page_0_0_value_2 + /*"; sailors: " + page_2_0_value_3 +*/ "; process (y/n): " + page_4_0_value_6 + "; offerReward (y/n): " + page_4_5_value_11 + "; " ); 
		
		DRYOBJ.Utils.evt_u.RetValFalprevDef( leEvt );
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		if( evtTrgt.id === "eleventhSet" ) { 
			var ar_InputRadio = evtTrgt.getElementsByTagName("input"), inputMax = ar_InputRadio.length, z = null;
			for( z = 0; z < inputMax; z = z + 1) {
				butThereIsAnIinWin12 = ConfirmField_VALUE( ar_InputRadio[ z ] ); 
				if( butThereIsAnIinWin12 && ar_InputRadio[ z ].className === "radioTwelve" ) {
					value12 = ar_InputRadio[ z ].value; // ( AKA busterCall )
					SetCookie( PAGE_4_5_1_VALUE_12, value12, COOKIE_LIFE );
				}					
			} // END for LOOP
			if( value12.length > 0 === true ) { 
				if( value12 === "yes" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_5_1_1_yes_i_am_enlisted.html"; 
				} 
				else 
				if( value12 === "no" ) { 
					window.alert("Please stand by, " + jollyrzerovalue + ", while your request is being processed.");
					window.location.href = "4_5_1_2_no_i_am_not_enlisted.html"; 
				} 					
			}
			else
			if( value12.length > 0 !== true  ) { 
				window.alert("Fill out one of the values, " + jollyrzerovalue + ". ");
			}
		}
	}
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_2_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */

	// var index_2_1 

	var A = null; 

	function Init_twoOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( A = 0; A < formMax; A = A + 1 ) { 
				if( ar_formOBJ[A].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[A], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_2_2_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_2_2_1
	var B = null;

	function Init_twoTwoOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( B = 0; B < formMax; B = B + 1 ) { 
				if( ar_formOBJ[B].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[B], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_2_2_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_2_2_2
	var C = null;

	function Init_twoTwoTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( C = 0; C < formMax; C = C + 1 ) { 
				if( ar_formOBJ[C].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[C], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_3_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_3_1
	var D = null;

	function Init_threeOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( D = 0; D < formMax; D = D + 1 ) { 
				if( ar_formOBJ[D].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[D], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_3_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_3_2
	var E = null;

	function Init_threeTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( E = 0; E < formMax; E = E + 1 ) { 
				if( ar_formOBJ[E].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[E], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_1_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_1_1
	var F = null; 

	function Init_fourOneOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( F = 0; F < formMax; F = F + 1 ) { 
				if( ar_formOBJ[F].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[F], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_1_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_1_2
	var G = null; 

	function Init_fourOneTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( G = 0; G < formMax; G = G + 1 ) { 
				if( ar_formOBJ[G].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[G], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_3  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_3
	var H = null;

	function Init_fourThree() { 
		if( navigator.cookieEnabled ) { 
			jollyrzerovalue = GetCookie( JOLLYRZEROVALUE ); 
			// console.log( "PIRATE or SAILOR?: " + jollyrzerovalue + "; " ); 
			
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( H = 0; H < formMax; H = H + 1 ) { 
				if( ar_formOBJ[H].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[H], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_4_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_4_1
	var J = null;

	function Init_fourFourOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( J = 0; J < formMax; J = J + 1 ) { 
				if( ar_formOBJ[J].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[J], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_4_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_4_2
	var K = null; 

	function Init_fourFourTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( K = 0; K < formMax; K = K + 1 ) { 
				if( ar_formOBJ[K].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[K], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_6  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_6
	var M = null; 

	function Init_fourSix() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( M = 0; M < formMax; M = M + 1 ) { 
				if( ar_formOBJ[M].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[M], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_2_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_2_2
	var N = null; 

	function Init_fourTwoTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( N = 0; N < formMax; N = N + 1 ) { 
				if( ar_formOBJ[N].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[N], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_2_1_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_2_1_1
	var P = null; 

	function Init_fourTwoOneOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( P = 0; P < formMax; P = P + 1 ) { 
				if( ar_formOBJ[P].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[P], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_2_1_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_2_1_2
	var Q = null; 

	function Init_fourTwoOneTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( Q = 0; Q < formMax; Q = Q + 1 ) { 
				if( ar_formOBJ[Q].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[Q], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_5_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_5_2
	var R = null; 

	function Init_fourFiveTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( R = 0; R < formMax; R = R + 1 ) { 
				if( ar_formOBJ[R].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[R], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_5_1_1  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_5_1_1
	var S = null; 

	function Init_fourFiveOneOne() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( S = 0; S < formMax; S = S + 1 ) { 
				if( ar_formOBJ[S].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[S], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  index_4_5_1_2  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	// index_4_5_1_2
	var T = null; 

	function Init_fourFiveOneTwo() { 
		if( navigator.cookieEnabled ) {
			ar_formOBJ = _docObj.getElementsByTagName( "form" ), formMax = ar_formOBJ.length; 
			for ( T = 0; T < formMax; T = T + 1 ) { 
				if( ar_formOBJ[T].id === "submit_GoHome" ) { 
					DRYOBJ.Utils.evt_u.AddEventoHandler( ar_formOBJ[T], "submit", Go_Home );
				}
			} 
		}
		if( !navigator.cookieEnabled ) { 
			window.alert( Marine_confObj.alerts.cookiesOn ); 
		}
	}
	// END of _private properties

	return {
		InitDCL: function() {
			return _DOMCONLO(); 
		} , // window.Gh_pages.InitDCL()
		InitLoad: function() {
			return _LOAD(); 
		} // window.Gh_pages.InitLoad()
	};
} )(); // window.Gh_pages

DRYOBJ.Utils.evt_u.AddEventoHandler( window , 'DOMContentLoaded' , Gh_pages.InitDCL() ); 
DRYOBJ.Utils.evt_u.AddEventoHandler( window, 'load' , Gh_pages.InitLoad() ); 